/**
 *
 * @type {{map: {"*": {jquerycookie: string}}}}
 */
var config = {
    map: {
        '*': {
            jquerycookie: "Kowal_ClientNotification/js/jquery.cookie.min",
            'customer-notify-modal': "Kowal_ClientNotification/js/customer.notify"
        }
    }
};