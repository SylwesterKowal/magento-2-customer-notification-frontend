define([
        "jquery",
        "Magento_Ui/js/modal/modal",
        "Kowal_ClientNotification/js/jquery.cookie.min"
    ], function ($) {
        var ExampleModal = {
            initModal: function (config, element) {
                var check_cookie = jQuery.cookie('customer_notiffy_popup');
                if (check_cookie == null || check_cookie != getCookeValue()) {
                    $target = $(config.target);
                    $target.modal();
                    $element = $(element);
                    $target.modal('openModal').on('modalclosed', function () {
                        jQuery.cookie('customer_notiffy_popup', getCookeValue(), {expires: 1});
                    });
                }
            }
        };
        return {
            'customer-notify-modal': ExampleModal.initModal
        };
    }
);

function getCookeValue() {
    return "nie-pokazuj-juz-dzisiaj";
}