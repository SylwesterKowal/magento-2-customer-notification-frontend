<?php


namespace Kowal\ClientNotification\Block;

class ClientNotification extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * ClientNotification constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return string
     */
    public function getClientNotofication()
    {
        return $this->getContent();
        // return __('Hello Developer! This how to get the storename: %1 and this is the way to build a url: %2', $this->_storeManager->getStore()->getName(), $this->getUrl('contacts'));
    }

    public function isEnable()
    {
        return $this->scopeConfig->getValue('notification/notification/notification_enable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    private function getContent()
    {
        return $this->scopeConfig->getValue('notification/notification/notification_content', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
